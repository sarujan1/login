import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './Component/Screens/Home';
import LoginForm from './Component/Screens/loginForm';

// type RootStackParamList = {
//   loginForm: undefined,
//   Home: undefined,
// }

const Stack = createNativeStackNavigator (); 

export default function App() {



return ( 

  <NavigationContainer> 
       <Stack.Navigator initialRouteName="LoginForm" screenOptions = {{ headerShown: false}}>
      <Stack.Screen name='LoginForm' component={LoginForm} />
        <Stack.Screen name='Home' component={Home} />
      </Stack.Navigator>
    </NavigationContainer>
);

    

}

