import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'

const SignInBtn = ({handleLogin, navigation}: any) => {
  return (

    <TouchableOpacity style={styles.loginBtn}
       onPress={handleLogin} 
      >

        <Text style = {styles.loginText}> LOGIN </Text> 
    </TouchableOpacity>
  )
}

export default SignInBtn

const styles = StyleSheet.create({
  loginBtn:
  {
    flexDirection: 'row',
    backgroundColor: "green",
    marginTop: 40,
    marginVertical: 10, // next text box space
    paddingHorizontal: 15, // Text space
    borderRadius: 10,
    // marginBottom: 30,

    // alignSelf: 'center',
    elevation: 10,
  },

  loginText: {
    fontSize: 20,
    marginLeft: 30, // Text set left
    padding: 8, // Text Box Height 
    fontWeight: 'bold',
  }
})