import { StyleSheet, Text, View, TextInput, SafeAreaView } from 'react-native'
import React, { useState } from 'react'
import { AntDesign, MaterialIcons, Octicons } from '@expo/vector-icons';

interface PasswordProps {
  placeholder: string;
  onFocus: () => void;
  onChangeText: (text: string) => void;
  secureTextEntry: boolean;
  iconname: string;
  clickSetColor: string;
  eyeOnPress: () => void;
  eyeName: string;
}

const Password = ({ placeholder, onFocus, onChangeText, 
                     secureTextEntry, iconname, clickSetColor, eyeOnPress, eyeName } : PasswordProps) => {

  return (

    <SafeAreaView style={styles.container}>
      
      <View style = {styles.inputView}>

          <View style={{flex:0.1}} >
            
          <MaterialIcons name = {iconname}   
              size={24}
              color = { clickSetColor } />
          </View>         
       
          <View style={{flex:0.8}}>

            <TextInput
              style={styles.TextInput}
              placeholder = {placeholder}
                onFocus = {onFocus} 

              secureTextEntry = { secureTextEntry } 
                 onChangeText={onChangeText}
            />
          </View>         

          
          <View style={{flex:0.1}}>
          
            <Octicons name = {eyeName} 
               size = {24}
               onPress = {eyeOnPress}
               style={{justifyContent:'flex-end'}} />

          </View>         



      </View> 

    </SafeAreaView>
  )
}

export default Password

const styles = StyleSheet.create({
  container:{
    flex: 1,
  },

  inputView: {
    flex:1,
    flexDirection: 'row',
    backgroundColor: "white",
    marginTop: 40,
    marginVertical: 10, // next text box space
    paddingHorizontal: 15, // Text space
    borderRadius: 10,
    // marginBottom: 30,

    // alignSelf: 'center',
    elevation: 20,

    justifyContent: 'flex-start',
    alignItems: 'center',
    

  },
  TextInput: {
    fontSize: 20,
    marginLeft: 2, // Text set left
    padding: 8 // Text Box Height 
  }
})