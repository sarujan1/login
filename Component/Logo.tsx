import { SafeAreaView, StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'

const Logo = ({ logoPic }:any) => {
  return (
    <SafeAreaView style = {styles.container}>

        <View style = {styles.logo1} >

           <Image source= {logoPic} 
           style={{alignSelf:'center',alignItems:'center', marginVertical:80, width:300,height:300,
           resizeMode:'contain', borderRadius:150, borderColor:'black', borderWidth:2
          }} />

        </View>

        
          
    </SafeAreaView>
  )
}

export default Logo

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
               
    },

    logo1: {
        backgroundColor: '#00FF00', // light Green
        // marginBottom: 20,
        
        borderRadius: 20
    },
})