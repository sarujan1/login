import { StyleSheet, Text, View, TextInput, SafeAreaView } from 'react-native'
import React, { useState } from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';

interface GmailProps {
  placeholder: string;
  onFocus: () => void;
  onChangeText: (text: string) => void;
  iconname: string;
  clickSetColor: string;
}



const Gmail = ({ placeholder, onChangeText, onFocus, iconname, clickSetColor }: GmailProps) => {

  return (

    <SafeAreaView style={styles.container}>

      <View style={styles.inputView}>
         {iconname && 
        <MaterialCommunityIcons name={iconname} size={24} color = {clickSetColor} />
         }

        <TextInput
          style={styles.TextInput}
          placeholder = {placeholder}
          onFocus={onFocus}
          onChangeText={onChangeText}
        />

      </View>

    </SafeAreaView>
  )
}

export default Gmail

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  inputView: {
    flexDirection: 'row',
    // width: '80%',
    backgroundColor: "white",
    marginTop: 50,
    marginVertical: 10, // next text box space
    paddingHorizontal: 10, // Text space
    borderRadius: 10,
    // alignSelf: 'center',
    elevation: 20,
    justifyContent: 'flex-start',
    alignItems: 'center',


  },
  TextInput: {
    fontSize: 20,
    marginLeft: 10,
    padding: 8 // Text Box Height 
  }
})