import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'

const ForgetPassword = () => {
  return (

      <TouchableOpacity>
        <Text style={styles.forgot_button}>Forgot Password?</Text> 
      </TouchableOpacity>

  )
}

export default ForgetPassword

const styles = StyleSheet.create({
    forgot_button: {
        alignSelf: 'center',
        height: 30,
        marginTop: 20,
        
       
      }
})