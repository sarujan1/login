import { SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import Logo from '../Logo';
import Gmail from '../Gmail';
import Password from '../Password';
import ForgetPassword from '../ForgetPassword';
import SignInBtn from '../SignInBtn';



const LoginForm = ({navigation}:any) => {

  const LogoImage = require('../../assets/Src/lk.jpg');


  const [emailFocus, setEmailFocus] = useState(false);
  const [passwordFocus, setPasswordFocus] = useState(false);

  const [showPassword, setShowPassword] = useState(false);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [error, setError] = useState('');


  const loginFunction = () => {
    console.log(email.toString().trim().length,'======================')
    
    if (email.toString().trim().length > 0 && password !== '') {
      console.log('Email:', email);
      console.log('Password:', password);
      navigation.navigate('Home', { UserName: email })
    } else {
      console.log('Please fill in both email and password');
    }
    
  }
    return (

        <ScrollView >
          {/* Logo */}
          <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 0.3 }}>
              <Logo logoPic={LogoImage} />
            </View>
            {/* Gmail */}
            <View style={{ flex: 0.2, paddingHorizontal: 50 }}>
    
              <Gmail
                placeholder="Email"
                // Click email then show red color
                onFocus={() => {
                  setEmailFocus(true)
                  setPasswordFocus(false)
                }}
                onChangeText={(value: string) => setEmail(value)}
    
                iconname="gmail"
                clickSetColor={emailFocus === true ? "red" : "black"}
    
              />
    
            </View>

            {/* Password Component */}
            <View style={{ flex: 0.2, paddingHorizontal: 50 }}>
              <Password
                placeholder="Password"
                onFocus={() => {
                  setPasswordFocus(true)
                  setEmailFocus(false)
                }}
                secureTextEntry={showPassword === false ? true : false}
    
                onChangeText={(password: string) => setPassword(password)}
    
                iconname="lock-outline"
                clickSetColor={passwordFocus === true ? "red" : "black"}
    
                eyeName={showPassword == false ? "eye-closed" : "eye"}
                eyeOnPress={() => setShowPassword(!showPassword)}
              />
    
            </View>
    
            <View style={{ flex: 0.1 }}>
              <ForgetPassword />
            </View>
    
            <View style={{ paddingHorizontal: 120, flex: 0.2 }}>
              <SignInBtn 
                 handleLogin = {loginFunction}
                //  {() =>  loginFunction()}
                 />
           
    
            </View>
    
          </SafeAreaView>
    
        </ScrollView>
      );
}

export default LoginForm

const styles = StyleSheet.create({})