import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const Home = ({ route, navigation }: any) => {
  const { UserName } = route.params;
  // console.log(email)

  return (

    <View style = {{flex: 1, backgroundColor: 'red'}}>
      
      <View style = {{flex: 0.2, backgroundColor: 'yellow', flexDirection: 'column'}}>
          <View style = {{flex: 0.5, backgroundColor: 'blue'}}/>
          
      </View>

      
      <View style = {{flex: 0.4, backgroundColor: 'pink'}}>
         <View style = {{flex: 0.5, backgroundColor: 'red'}}>
            <Text style = {styles.email}> Email : {UserName} </Text>
         </View>
      </View>

      <View style = {{flex: 0.4, backgroundColor: 'green', flexDirection: 'row'}}>
         <View style = {{flex: 0.33, backgroundColor: 'yellow'}}/>
         <View style = {{flex: 0.33, backgroundColor: 'white'}}/>
      </View>
     
    </View>

  )
}

export default Home

const styles = StyleSheet.create({
  email: {
    width: 80,
    alignSelf: 'center',
    alignItems: 'center',
    fontSize: 25,
  }
})